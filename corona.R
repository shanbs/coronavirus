library(magrittr)

render = function() {
  get_trajectories_png()
  get_trajectories_png(deceased = TRUE)
}

get_trajectories_png = function(deceased = FALSE) {
  url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/"

  filename = ifelse(deceased, 'deaths', 'confirmed') %>%
    paste0('time_series_covid19_', ., '_global.csv')

  system2('wget', paste0(url, filename))
  df_csv = read.csv(filename)

  df_csv$Country.Region %<>% gsub('US', 'United States', .)
  df_csv$Country.Region %<>% gsub('Korea, South', 'South Korea', .)

  df_csv %>% split(.$Country.Region) %>%
    lapply(get_country_trajectory, ifelse(deceased, 10, 100)) %>%
    cowplot_trajectories(deceased) %>%
    png_ggplot(paste0('coronavirus_', ifelse(deceased, 'deaths', 'cases'),
      '_trajectories'), 1200, 800)
}

get_country_trajectory = function(df_country, baseline = 100) {
  sums = colSums(df_country[-c(1:4)]) 
  above_baseline = sums >= baseline
  cbind(if (any(above_baseline)) sums[above_baseline] else 0) %>%
    `rownames<-`(seq_len(nrow(.)) - 1)
}

get_top_trajectories = function(l_trajectories, n_top = 20) {
  df_ranks = sapply(l_trajectories, tail, 1) %>%
    sort %>% tail(n_top) %>% rank %>%
    cbind.data.frame(country = names(.), rank = ., stringsAsFactors = FALSE)

  l_trajectories[df_ranks$country] %>% reshape2::melt() %>%
    setNames(c('day', 'varname', 'cases', 'country')) %>%
    merge(df_ranks)
}

format_labels = function(x) format(x, big.mark = ',', scientific = FALSE)

trajectories_labels = function(deceased, lang) {
  deceased = ifelse(deceased, 'death', 'case')
  labels = switch(lang, en = {
      list(paste('How coronavirus', deceased,
          'trajectories compare between countries'),
        paste0('Cumulative number of ', deceased,
          's of most affected countries, on'),
        'Data: John Hopkins University - Code: gitlab.com/thomaschln/coronavirus',
        paste('Number of days since',
          switch(deceased, death = '10th', case = '100th'), deceased))
    }, fr = {
      list("Comment les trajectoires de l'épidemie de coronavirus diffèrent par pays",
        'Nombre cumulé de cas dans les pays les plus touchés, le',
        'Données: Université John Hopkins - Code: gitlab.com/thomaschln/coronavirus')
    }) %>% setNames(c('title', 'subtitle', 'caption', 'x'))

  linear = labels %$%
    ggplot2::labs(x = x, title = title, caption = "",
      subtitle = paste(subtitle, Sys.Date(), '\n'))
  logarithmic = labels %$% 
    ggplot2::labs(x = x, title = '', subtitle = '\n', caption = caption)

  list(linear = linear, logarithmic = logarithmic)
}
  
cowplot_trajectories = function(l_trajectories, deceased = FALSE,
  lang = c('en', 'fr'), combine_linear = TRUE) {

  labels = trajectories_labels(deceased, match.arg(lang))

  df_trajectories = get_top_trajectories(l_trajectories, 20)
  gg_log = ggplot_trajectories(df_trajectories) +
    labels$logarithmic + ggplot2::scale_y_log10(labels = format_labels)

  if (!combine_linear) return(gg_log)

  colors = ggplot2::ggplot_build(gg_log)$data[[3]] %$% setNames(colour, label)
  gg_lin = ggplot_trajectories(subset(df_trajectories, rank > 10), colors) +
    labels$linear + ggplot2::scale_y_continuous(labels = format_labels)

  cowplot::plot_grid(nrow = 1, labels = c('Linear scale', 'Logarithmic scale'),
    label_x = 0.02, label_y = .925, hjust = 0, label_fontface = 'plain',
    gg_lin, gg_log)
}

ggplot_trajectories = function(df_series, colors = NULL) {
  set.seed(0)
  df_labels = df_series %>% dplyr::group_by(country) %>%
    dplyr::summarize(day = dplyr::last(day), cases = dplyr::last(cases))

  gg = ggplot2::ggplot(df_series, ggplot2::aes(day, cases, color = country)) +
    ggplot2::geom_line(alpha = .5) + ggplot2::geom_point(alpha = .5) +
    ggrepel::geom_label_repel(
      ggplot2::aes(day, cases, label = country, color = country), 
      df_labels, size = 5, direction = 'x', nudge_x = 20, force = 10,
      segment.color = 'grey') +
    ggplot2::geom_point(ggplot2::aes(day, cases, fill = country), df_labels,
      shape = 21, size = 3, color = 'black') +
    cowplot::theme_cowplot(font_size = 20) +
    ggplot2::theme(axis.title.y = NULL, legend.position = "none") +
    ggplot2::labs(y = '')

  if (is.null(colors)) {
    gg + ggplot2::scale_color_hue(l = 40) + ggplot2::scale_fill_hue(l = 40)
  } else {
    gg + ggplot2::scale_color_manual(values = colors) +
      ggplot2::scale_fill_manual(values = colors)
  } 
}

png_ggplot = function(widget, filename, width = 800, height = 800) {
  png(paste0(filename, '.png'), width, height)
  print(widget)
  dev.off()
}
