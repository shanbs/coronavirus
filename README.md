# coronavirus

This small R script renders coronavirus case trajectories by countries using [data from the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University](https://github.com/CSSEGISandData/COVID-19), as [published by Jonh Burn-Murdoch in the Financial Times](https://www.ft.com/content/a26fbf7e-48f8-11ea-aeb3-955839e06441) and [reproduced in R by Pieter Provoost](https://github.com/pieterprovoost/coronavirus-notebook). 

![](coronavirus_cases_trajectories.png)

![](coronavirus_deaths_trajectories.png)
